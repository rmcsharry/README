1. Introduction

   1. Computers are Just Things That Can Follow Instructions
   1. Programming: Telling A Computer What to Do
   1. Developing: Telling Humans What a Computer Should Do
   1. Compiling Human Instructions into Machine Language
   1. A Note About the "Binary Language of Moisture Evaporators"

   1. Organizing Your Instructions
      1. Everything is a Thing
      1. Things Have Types and Properties
      1. Things Do Stuff 
      1. Things React to Events and Environment
      1. Things Work Together
         1. One Thing at at Time
         1. At the Same Time

   1. The Go Programming Language
      1. Why Go?
         1. Designed, Not Accidental
         1. Practical and Employable
         1. Strictly Typed-ish
      1. Getting Go
         1. <https://go.dev>
         1. Install Locally
         1. Use REPL.it Online

1. Things Have Properties with Values

   1. A Byte of Bits: It's All Just Ones and Zeroes
   1. Values Have Types (It's How You Look at It)
   1. Numbers
      1. Integers / Whole
      1. Floats / Decimals
      1. Complex
      1. *All Your Base Are Belong to Us*
         1. Decimal
         1. Binary
         1. Hexadecimal
         1. Octal 
         1. Base64
   1. Characters are Just Numbers (Runes, Unicode)
   1. Strings
   1. Constants
   1. Collections
      1. Slices (Arrays, Lists)
      1. Maps (Dictionaries)
   1. Exotic Types
      1. Pointers
      1. Empty Interfaces
      1. Iota
      1. Channels
   1. Defining a Custom Type
      1. Structures

1. Things Do Stuff

   1. Actions
   1. Flow Control
   1. Functions (Just Another Type)
   1. Operations and Methods

1. Things Work Together

   1. Interfaces
   1. Channels
   1. Protocols

1. Things React to Events and Environments

1. Thing Go Boom

1. Contributors
   * [\@spydaw](https://twitch.tv/spydaw)

