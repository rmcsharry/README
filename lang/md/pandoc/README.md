---
Title: Pandoc Markdown
Subtitle: World's Best Syntax for Knowledge Source
Query: true
---

:::co-warning
If you have not yet mastered [Basic Markdown](/lang/md/basic/) start there.
:::

[Pandoc](/tools/pandoc/) has been the academic standard for capturing knowledge source in the form of textbooks, web sites, slides, ePubs, and more for more than a decade. It is the *only* widely adopted [Markdown](/lang/md/) that fully support LaTeX, the language of mathematics. As such, it is critically important that everyone learn it who wishes to capture, manage, and share their knowledge for whatever purpose.

:::co-fyi
The R language project liked Pandoc Markdown so much it adopted it as the standard documentation format for the language itself.
:::

## See Also

* [Pandoc Markdown Official Documentation](https://pandoc.org/MANUAL.html#pandocs-markdown)
