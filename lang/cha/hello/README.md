---
Title: '"Hello World" Challenge'
Tags: easy
---

Create program that prints `Hello World!` when run from the
command line or console locally. 

## Requirements

* Must include a *safe* shebang line if a script.
* When `./hello` then prints `Hello World!`.

## Bonus

* Use a variable assigned `World` as a value.
* Validate using language test framework.
