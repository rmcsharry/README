---
Title: Other Related Streamers
Subtitle: More Than One Way to Tune In
---

Even though [rwxrob.live](https://rwxrob.live) is the primary stream --- for now --- here are some other streamers you should follow who regularly stream about related topics. Some are experienced veterans, others are just working through the course themselves. All of them stream on Twitch. 

:::co-fyi
To add yourself [submit a ticket](https://gitlab.com/rwx.gg/README/-/issues) (or better yet a merge request) with your request to be added.
:::

  Twitch ID                        Description
---------------------------------  ------------------------------------------------
 [\@elremingu](https://twitch.tv/elremingu)                        Networking
 [\@rwxrob](https://twitch.tv/rwxrob)                           Linux Termimal Master, Bash, Markdown, Go
 [\@ltn_bob](https://twitch.tv/ltn_bob)                          Network engineering instructor
 [\@iScreaMan23](https://twitch.tv/iScreaMan23)                      Math and Physics 
 [\@zerostheory](https://twitch.tv/zerostheory)                      
 [\@apostolique](https://twitch.tv/apostolique)

