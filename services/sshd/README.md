---
Title: Secure Shell Server Daemon Service
Subtitle: The Thing You `ssh` Connect *To*
---

The *secure shell* protocol is a safe way to connect from one computer to another. The server daemon that listens on port 22 for incoming connections is called `sshd` usually. Is is rarely enabled or installed by default.
