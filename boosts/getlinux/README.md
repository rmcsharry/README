---
Title: Get Me Linux, Stat!
Subtitle: Understand All the Ways to Get Linux
Query: true

Summary:
  It's still pretty uncommmon to be able to buy a computer that comes with Linux. So how do you get it? This boost introduces all the different ways you can get Linux and start using it right way including buying a computer with Linux installed already, running Linux like an app on your current operating system, creating a remote Linux on a cloud hosting service that you can access over the Internet with secure shell from the terminal, and blowing away a computer's old OS and replacing it with Linux.

Type: Summaries

---

1. [Preloaded Hardware]
   1. [Single-Board Computers]
      1. [Raspberry Pi]
   1. [Computers]
      1. [System76]
      1. [Purism]
      1. [Lenovo]
      1. [Dell]
1. [Replace Hardware OS] 
   1. [Download an Image]
   1. [Flash Image to USB]
   1. [Boot from USB]
   1. [Run Installer]
1. [Dual Boot] 
1. [Live Boot]
1. [Locally Hosted]
   1. [Virtual Machines]
      1. [Windows WSL2]
      1. [Virtual Box]
      1. [VMware]
   1. [Containers]
      1. [Docker]
1. [Remotely Hosted]
   1. [PicoCTF]
   1. [Digital Ocean]
   1. [Linode]
   1. [Amazon]
   1. [Azure]
   1. [Google]
1. [Linux from Scratch]

<!-------------------------------------->

## Preloaded

### Single-Board Computers

#### Raspberry Pi

## Personal Computers

### System76

### Purism

### Lenovo

### Dell

## Replace Host OS

### Distro Images

### Downloading Images

### Flashing Images

### Boot from USB

### Run Installer

## Live Boot

## Hosted

### Local

#### Virtual Machines

##### Windows WSL

##### Virtual Box

##### VMware

#### Containers

##### Docker

### Remote

#### PicoCTF

#### Digital Ocean

#### Linode

#### Amazon

#### Azure

#### Google

