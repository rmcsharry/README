---
Title: Windows Subsystem for Linux vs Virtual Machine?
---

## Virtual Machine

* Requires installing VM engine
* Simulates what will *eventually* run on hardware
* More stable
* Requires a beefy computer
* Industry best practice for testing
  * OSes
  * Software
  * Networking
  * Sandbox when combined with VPN
  * Fuzzing

## Windows Subsystem for Linux

* Requires installation procedure
* Doesn't allow tricking out terminal as much
* Has cut and paste quirks
