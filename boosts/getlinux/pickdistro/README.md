---
Title: Pick a *Beginner* Linux Distro for You
Subtitle: Why Debian-Based is Best
Summary:
  Recommendations based on the most common needs most Linux beginners have, not what someone happens to find to be their favorite.
---

1. [Beginner Needs and Wants]
   1. [Easy to Setup]
   1. [Easy to Install Software]
   1. [*Helpful* Community]
   1. [Builds Core Skills]
      1. [Likely to be on Certification]
      1. [Required by Most Employers]
   1. [Terminal User Interface]
   1. [Secondary Emphasis on Graphics]
   1. [Teriary Emphasis on Desktop and Window Managers]
   1. [Enjoyable]
1. [Debian-Based]
   1. [WSL2 Ubuntu]
   1. [PopOS!]
   1. [Linux Mint]

