---
Title: Like Mentality
Subtitle: Loving the Likes, Not Learning
Query: true
---

*Like mentality* is judging the quality of something solely by how popular it is, by how many likes, favs, up-votes, and claps. This is extremely dangerous. StackExchange, for example, is *full* of overwhelmingly bad information that is somehow the most popular. Innovative thinking is *never* popular when it begins. In fact, it is often scorned and shunned --- even persecuted. The end result has been portrayed in [The Orville](https://duck.com/lite?kae=t&q=The Orville) and other dystopian visions of a future none of us want. 

:::co-tip
The more likely you are to read something on the Internet, the less likely that thing is to be true or useful. People who make time to market their writing are usually *not* the people you want to listen to. Unfortunately, the people you *do* want to learn from are too busy getting stuff done to share it. Find *them* and you cannot go wrong. Innovation happens in quiet corners, not in loud flame wars on Twitter, Reddit, Ycombinator, [Dev.to](/advice/dont/dodevto) and the rest of the time-wasting tech "forums." 

:::

No matter how many people repost a false quote it will never become true, until it does. In fact, this is how urban legends, myths, and entire religions get started, from the burning collective desire for something to be true that just isn't.
