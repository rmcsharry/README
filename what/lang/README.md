---
Title: Computer Languages
Subtitle: Including the Binary Language of Moisture Evaporators
Query: true
---

A *computer language* usually refers to a programming or markup language. These languages have grammar including [syntax](/what/syntax/) just like other natural human languages. In fact, those who study foreign language tend to pick up new programming languages easier as well, and vice versa.

## Major Categories

Computer languages can be categorized in dozens of ways but ultimately fall into the following three major categories:

--------------  -------------------------------------- 
 Category       Description
--------------  -------------------------------------- 
 Imperative     Contain instructions or
                procedures telling the computer what
                to do and *how* to do it requiring
                the computer to remember what it is
                doing as it does it called *state*.

 Declarative    Contain descriptions of *what* is
                wanted from the computer by describing
                it rather then telling it what to do.

 Data           Data languages are not programming at
                all but critically important since
                they contain structured data used
                by other languages. There is often
                a fine line between declarative and
                data languages.
--------------  -------------------------------------- 

One of the biggest mistakes in the minds of computer programmers is assuming that *only* imperative programming *is* programming. You see mistakes even in curriculums like Harvard that state emphatically that HTML is not a *programming* language, which is categorically wrong, [it is](https://youtu.be/4A2mWqLUpzw), it's a *declarative* programming language.

:::co-fyi
There are dozens of other [computer language  paradigms](https://duck.com/lite?kae=t&q=computer language  paradigms). These are just the two main categories.
:::

## See Also

* [HTML *IS* a Programming Language](https://youtu.be/4A2mWqLUpzw)
* [Recommended First Languages to Learn](/lang/)
* [Java is *Not* Object-Oriented Programming](https://youtu.be/ZrBQmIDdls4?t=718)
* [The Object-Oriented "Darkness Across the Land](https://youtu.be/HgtRAbE1nBM?t=1341)

