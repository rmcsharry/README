---
Title: 'What is *The Bench* in Tech?'
Subtitle: At the Game, But Not Playing
Query: true
---

*The bench* in the tech industry is a colloquial term for being employed or on a team but not being called on to really do anything. Sometimes you can actually put yourself on the bench without even knowing it. One thing is for sure, the only way you get off the bench is by getting laid off or getting engaged again. Often you need to get *yourself* off the bench.

## See Also

* [Proactive Proof of Engagement](/advice/engage)
